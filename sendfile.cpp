/*
  Tugas Besar 1 IF 3130 Jaringan Komputer
  Sliding Window Protocol
  Anggota :
  1. 13515024 - Abdurrahman
  2. 13515069 - Hisham Lazuardi Yusuf
  3. 13515099 - Mikhael Artur Darmakesuma

  Nama File : sendfile.cpp

*/

#include <bits/stdc++.h>
#include <string>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#include "sendfile.h"

using namespace std;

#define BUFLEN 512
#define PORT 8080
#define SERVER "127.0.0.1"

int main() {
  //Variabel
  struct sockaddr_in si_other;
  int sock, i;
  unsigned int slen = sizeof(si_other);
  char buf[BUFLEN];
  char message[BUFLEN];
  bool check = false;
  string line;

  if ( (sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1 ) {
    cout << "socket not available" << endl;
  } else {
    // cout << "success 1" << endl;
  }

  memset((char *) &si_other, 0, sizeof(si_other));
  si_other.sin_family = AF_INET;
  si_other.sin_port = htons(PORT);

  if (inet_aton(SERVER, &si_other.sin_addr) == 0) {
    cout << "inet_aton() failed" << endl;
    exit(1); //optional
  } else {
    // cout << "success 2" << endl;
  }

  //file i/o trial
  FILE *inf;
  inf = fopen("test.txt", "r");

  while (fgets(message, 9, (FILE *) inf) != NULL) {

    cout << "Message : " << message << endl;

    // cout << "Enter message : ";
    // cin >> message;
    if (sendto(sock, message, strlen(message), 0, ((struct sockaddr*) &si_other), slen) == -1) {
      cout << "sendto()" << endl;
    } else {
      // cout << "success 3" << endl;
    }
    memset(buf, '\0', BUFLEN);
    if (recvfrom(sock, buf, BUFLEN, 0, ((struct sockaddr*) &si_other), &slen) == -1) {
      cout << "recvfrom()" << endl;
    } else {
      // cout << "success 4" << endl;
    }
    // puts(buf);
    // cout << buf << endl;
  }

  close(sock);
  return 0;
}
