/*
  Tugas Besar 1 IF 3130 Jaringan Komputer
  Sliding Window Protocol
  Anggota :
  1. 13515024 - Abdurrahman
  2. 13515069 - Hisham Lazuardi Yusuf
  3. 13515099 - Mikhael Artur Darmakesuma

  Nama File : receivefile.cpp

*/
#include <bits/stdc++.h>
#include <string>
#include <stdlib.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
// #include <netinet/in.h>

// #include "receivefile.h"

using namespace std;

#define BUFLEN 512
#define PORT 8080
#define SERVER "127.0.0.1"

int main() {
  struct sockaddr_in si_me, si_other;

  int s, i, recv_len;
  unsigned int slen = sizeof(si_other);
  char buf[BUFLEN];

  //create a UDP socket
  if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
  {
      cout << "socket not available" << endl;
  }

  // zero out the structure
  memset((char *) &si_me, 0, sizeof(si_me));

  si_me.sin_family = AF_INET;
  si_me.sin_port = htons(PORT);
  si_me.sin_addr.s_addr = htonl(INADDR_ANY);

  //bind socket to port
  if( bind(s , (struct sockaddr*)&si_me, sizeof(si_me) ) == -1)
  {
      // die("bind");
  }

  //keep listening for data
  while(1)
  {
      // cout << ("Waiting for data...") << endl;
      printf("Waiting for data...\n");
      fflush(stdout);

      //try to receive some data, this is a blocking call
      if ((recv_len = recvfrom(s, buf, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) == -1)
      {
          cout << "receive failed" << endl;
      }

      //print details of the client/peer and the data received
      cout << "Received packet from " << inet_ntoa(si_other.sin_addr) << ":" << ntohs(si_other.sin_port) << endl;
      // printf("Received packet from %s:%d\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
      cout << "Data: " << buf << endl;

      //now reply the client with the same data
      if (sendto(s, buf, recv_len, 0, (struct sockaddr*) &si_other, slen) == -1)
      {
          cout << "send failed" << endl;
      }
  }

  close(s);

  return 0;
}
